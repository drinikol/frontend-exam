import React from 'react';
import {
    Box, Paper,
    Button, Typography
} from '@mui/material';
import { useSelector, useDispatch } from "react-redux"
import { userRemoved } from '../redux/actions';
export default function Warning(props) {
    const SELECTED_USER = useSelector(state => state.selectedUser)
    const dispatch = useDispatch()
    const mainBoxStyle = {
        height: 0.1,
        width: 0.4,
        backgroundColor: 'background.default',
        my: 5
    }
    const buttonsContainer = {
        py: 2,
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        width: 1
    }
    const buttonStyle = {
        display: "flex",
        flexDirection: "row",
        mx: 1
    }

    const deleteUser = (e) => {
        e.stopPropagation() //handle rendering issue with table
        dispatch(userRemoved(SELECTED_USER))
        props.cancel(false)
    }
    return (
        <Box sx={mainBoxStyle}>
            <Paper variant="outlined" sx={{ py: 5, px: 4 }}>
                <Box sx={buttonsContainer}>
                    <Typography variant="h5" textAlign="center">
                        {
                            `Are you sure you want to DELETE this user?`
                        }
                    </Typography>
                </Box>
                <Box sx={buttonsContainer}>
                    <Typography variant="h4" textAlign="center">
                        {
                            `${SELECTED_USER.name}`
                        }
                    </Typography>
                </Box>
                <Box sx={buttonsContainer}>
                    <Box sx={buttonStyle}>
                        <Button variant="contained" onClick={() => props.cancel(false)} sx={{ backgroundColor: "gray" }} >Cancel</Button>
                    </Box>
                    <Box sx={buttonStyle}>
                        <Button variant="contained" onClick={(e) => deleteUser(e)} color="error">Submit</Button>
                    </Box>
                </Box>
            </Paper>
        </Box >

    )
}