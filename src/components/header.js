import {
    Box, Typography
} from '@mui/material';
export default function HeaderComponent(props) {
    return (
        <Box sx={{ minHeight: '1vh', height: "1vh", width: 1, px: 2, py: 3 }} >
            <Typography variant="h4">
                {props.children}
            </Typography>
        </Box>
    )
}