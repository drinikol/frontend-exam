import React from 'react';
import { useFormik } from 'formik';
import * as yup from 'yup';
import {
    Box, Paper,
    Button, TextField, Typography
} from '@mui/material';
import { makeStyles, createStyles } from "@mui/styles";
import { useSelector } from "react-redux"
export default function UserForm(props) {

    const SELECTED_USER = useSelector(state => state.selectedUser)

    const validationSchema = yup.object({
        email: yup
            .string('Enter your email')
            .email('Enter a valid email')
            .required('Email is required'),
        name: yup
            .string('Enter your name')
            .required('Name is required'),
        username: yup
            .string('Enter a Username'),
        city: yup
            .string('Enter your City')
    });
    const onHandleSubmit = (values) => {
        if (props.editUser) {
            props.addFunction({ ...values, id: SELECTED_USER.id })
        } else {
            props.addFunction(values)
        }
    }
    const formik = useFormik({
        initialValues: {
            email: props.editUser ? SELECTED_USER.email : "",
            name: props.editUser ? SELECTED_USER.name : "",
            username: props.editUser ? SELECTED_USER.username : "",
            city: props.editUser ? SELECTED_USER.address.city : ""
        },
        enableReinitialize: true,
        validationSchema: validationSchema,
        onSubmit: onHandleSubmit,
    });

    const useStyles = makeStyles((theme) =>
        createStyles({
            mainBoxStyle: {
                minHeight: '20vh',
                height: "20vh",
                width: "30%",
                marginTop: "5%",
                backgroundColor: 'background.default',
                my: 5,
                [theme.breakpoints.down('md')]: {
                    width: "50%"
                },
                [theme.breakpoints.down("sm")]: {
                    width: "90%",
                },
                // [theme.breakpoints.up("md")]: {},
                // [theme.breakpoints.up("lg")]: {},
            },
            paperContainer: {
                display: "flex",
                flexDirection: "column",
                padding: "5%",
                justifyContent: "center",
            },
            inputfieldStyle: {
                marginTop: "5%",
            },
            buttonsContainer: {
                display: "flex",
                flexDirection: "row",
                justifyContent: "flex-end",
                marginTop: "5%",
                width: "100%",
                [theme.breakpoints.down("sm")]: {
                    alignItems: "center",
                    alignSelf: "center",
                    justifyContent: "space-evenly",
                },
            },
            buttonStyle: {
                display: "flex",
                flexDirection: "row",
                marginLeft: "5%",
                [theme.breakpoints.down("sm")]: {
                    alignItems: "center",
                    alignSelf: "center",
                    justifyContent: "space-evenly",
                    marginLeft: 0
                },
            }
        })
    );
    const styles = useStyles();
    return (
        <Box className={styles.mainBoxStyle}>
            <Paper variant="outlined" className={styles.paperContainer}>
                <Typography variant="h6">
                    {
                        props.editUser ? `Edit User` : `Add User`
                    }
                </Typography>
                <form onSubmit={formik.handleSubmit}>
                    <Box className={styles.inputfieldStyle}>
                        <TextField
                            fullWidth
                            id="email"
                            name="email"
                            label="Email"
                            value={formik.values.email}
                            onChange={formik.handleChange}
                            error={formik.touched.email && Boolean(formik.errors.email)}
                            helperText={formik.touched.email && formik.errors.email}
                        />
                    </Box>
                    <Box className={styles.inputfieldStyle}>
                        <TextField
                            fullWidth
                            id="name"
                            name="name"
                            label="Name"
                            value={formik.values.name}
                            onChange={formik.handleChange}
                            error={formik.touched.name && Boolean(formik.errors.name)}
                            helperText={formik.touched.name && formik.errors.name}
                        />
                    </Box>
                    <Box className={styles.inputfieldStyle}>
                        <TextField
                            fullWidth
                            id="username"
                            name="username"
                            label="Username"
                            value={formik.values.username}
                            onChange={formik.handleChange}
                            error={formik.touched.username && Boolean(formik.errors.username)}
                            helperText={formik.touched.username && formik.errors.username}
                        />
                    </Box>
                    <Box className={styles.inputfieldStyle}>
                        <TextField
                            fullWidth
                            id="city"
                            name="city"
                            label="City"
                            value={formik.values.city}
                            onChange={formik.handleChange}
                            error={formik.touched.city && Boolean(formik.errors.city)}
                            helperText={formik.touched.city && formik.errors.city}
                        />
                    </Box>
                    <Box className={styles.buttonsContainer}>
                        <Box className={styles.buttonStyle}>
                            <Button variant="contained" color="error" onClick={() => props.cancel(false)}>Cancel</Button>
                        </Box>
                        <Box className={styles.buttonStyle}>
                            <Button variant="contained" type="submit" color="success">Submit</Button>
                        </Box>
                    </Box>
                </form>
            </Paper>
        </Box >

    )
}