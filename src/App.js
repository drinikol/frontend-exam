import { useState, useEffect } from "react"
import axios from 'axios';
import { useSelector, useDispatch } from "react-redux"
import {
  userAdded, userUpdated,
  usersAdded, selectedUser,
  sortUsersAZ, sortUsersZA
} from "./redux/actions"

import {
  Box, Button,
  Typography

} from '@mui/material';
import { makeStyles, createStyles } from "@mui/styles";
import { DataGrid } from '@mui/x-data-grid';
import UserForm from "./components/userForm";
import Warning from "./components/warning";
import HeaderComponent from "./components/header";
function App() {
  const [isModalShowing, setIsModalShowing] = useState(false)
  const [editUser, setEditUser] = useState(false)
  const [showWarning, setShowWarning] = useState(false)
  const [pageSize, setPageSize] = useState(5)
  const [isASC, setIsASC] = useState(true)
  const SAVED_USERS = useSelector(state => state.users)
  const dispatch = useDispatch()

  useEffect(() => {
    const fetchInitialUsers = async () => {
      const result = await axios('https://my-json-server.typicode.com/karolkproexe/jsonplaceholderdb/data')
      if (result.data && result.data.length > 0) {
        const USERS_DATA = result.data
        dispatch(usersAdded(USERS_DATA))
      }
    }
    fetchInitialUsers()
  }, [])
  const generateID = () => (
    Math.ceil(Math.random() * 100) + Math.floor(Math.random() * 100)
  )
  const addUserFunction = (params) => {
    if (params.id) {
      dispatch(userUpdated(params))
    } else {
      dispatch(userAdded({ id: generateID(), ...params, }))
    }
    setEditUser(false)
    setIsModalShowing(false)
  }
  const editUserFunction = (params) => {
    dispatch(selectedUser(params))
    setEditUser(true)
    setIsModalShowing(true)
  }
  const deleteUserFunction = (params, e) => {
    dispatch(selectedUser(params))
    setShowWarning(true)
  }
  const usernameOrderHandler = (ascOrder) => {
    if (ascOrder) {
      dispatch(sortUsersAZ())
    } else {
      dispatch(sortUsersZA())
    }
    setIsASC(!ascOrder)
  }
  const EditButton = (params) => {
    return (
      <strong key={params.id}>
        <Button
          variant="contained"
          style={{ marginLeft: 16 }}
          color="warning"
          onClick={() => editUserFunction(params.row)}
        >
          Edit
        </Button>
      </strong>
    )
  }
  const DeleteButton = (params) => {
    return (
      <strong key={params.id}>
        <Button
          variant="contained"
          style={{ marginLeft: 16 }}
          color="error"
          onClick={(e) => deleteUserFunction(params.row, e)}
        >
          Delete
        </Button>
      </strong>
    )
  }
  const columns = [
    {
      field: 'id',
      headerName: 'ID',
      width: 90,
      sortable: false,
      disableColumnMenu: true,
    },
    {
      field: 'name',
      headerName: 'Full Name',
      width: 220,
      editable: true,
      sortable: false,
      disableColumnMenu: true,
    },
    {
      field: 'username',
      headerName: 'Username',
      width: 150,
      editable: true,
    },
    {
      field: 'email',
      headerName: 'Email',
      width: 220,
      editable: true,
      sortable: false,
      disableColumnMenu: true,
    },
    {
      field: 'city',
      headerName: 'City',
      minWidth: 200,
      sortable: false,
      disableColumnMenu: true,
      editable: true,
      valueGetter: (params) => {
        if (params.row.address) {
          if (params.row.address.city) {
            return params.row.address.city
          }
        } else {
          return "N/A"
        }
      }
    },
    {
      field: "Edit",
      minWidth: 120,
      width: 120,
      editable: true,
      sortable: false,
      disableColumnMenu: true,
      renderCell: EditButton,
    },
    {
      field: "Delete",
      minWidth: 120,
      width: 120,
      editable: true,
      sortable: false,
      disableColumnMenu: true,
      renderCell: DeleteButton,
    }
  ];

  const useStyles = makeStyles((theme) =>
    createStyles({
      mainContainer: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        height: "100%",
        padding: "1%",
        [theme.breakpoints.down("sm")]: {

        }
      },
      modalBG: {
        display: "flex",
        backgroundColor: "rgba(0,0,0,0.55)",
        position: "fixed",
        left: 0,
        right: 0,
        bottom: 0,
        top: 0,
        justifyContent: "center",
        zIndex: 1000,
      },
      bodyContainer: {
        display: "flex",
        flexDirection: "column",
        height: "90vh",
        width: "50%",
        [theme.breakpoints.down("sm")]: {
          width: "90%",
        }
      },
      gridContainer: {
        display: "flex",
        flexGrow: 1,

        [theme.breakpoints.down("sm")]: {

        }
      },
      headerOneContainer: {
        display: "flex",
        justifyContent: "center",
        flexDirection: "row",
        height: "4vh",

        [theme.breakpoints.down("sm")]: {
          flexDirection: "column",
          alignItems: "center",
          height: "10vh"
        }
      },
      headerTwoContainer: {
        display: "flex",
        //   justifyContent: "center",
        flexDirection: "row",
        height: "6vh",
        minWidth: "100%",
        [theme.breakpoints.down("sm")]: {

          alignItems: "center",
          height: "10vh"
        }
      },
      headerTwoBox1: {
        display: "flex",
        flexDirection: "row",
        flex: 8,
        alignItems: "center",
        [theme.breakpoints.down("sm")]: {
          flex: 1,

        }
      },
      headerTwoBox2: {
        display: "flex",
        flexDirection: "row",
        flex: 2,
        alignItems: "center",
        justifyContent: "flex-end",
        [theme.breakpoints.down("sm")]: {
          flex: 1,
        }
      },
      titleTextContainer: {
        [theme.breakpoints.down("sm")]: {
          minWidth: 100
        }
      },
      titleText: {

      },
      sortButtonContainer: {
        marginLeft: 10
      },

      inputfieldStyle: {
        marginTop: "5%",
      },
      buttonsContainer: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "flex-end",
        marginTop: "5%",
        width: "100%",
        [theme.breakpoints.down("sm")]: {
          alignItems: "center",
          alignSelf: "center",
          justifyContent: "space-evenly",
        },
      },
      buttonStyle: {
        display: "flex",
        flexDirection: "row",
        marginLeft: "5%",
        [theme.breakpoints.down("sm")]: {
          alignItems: "center",
          alignSelf: "center",
          justifyContent: "space-evenly",
          marginLeft: 0
        },
      }
    })
  );
  const styles = useStyles();
  return (
    <Box className={styles.mainContainer}>
      {
        isModalShowing && <Box className={styles.modalBG}>
          <UserForm addFunction={addUserFunction} cancel={setIsModalShowing} editUser={editUser} />
        </Box>
      }
      {
        showWarning && <Box className={styles.modalBG}>
          <Warning cancel={setShowWarning} />
        </Box>
      }
      <Box className={styles.bodyContainer}>
        <Box className={styles.headerOneContainer} >
          <Typography variant="h5" textAlign="center" sx={{ fontWeight: "bold", color: "rgba(100,200,200,1)" }} >Dashboard</Typography>
        </Box>
        <Box className={styles.headerTwoContainer} >
          <Box className={styles.headerTwoBox1}>
            <Box className={styles.titleTextContainer}>
              <Typography className={styles.titleText}
                variant="h6"
                textAlign="center"
                sx={{ fontWeight: 400, color: "rgba(255,100,100,1)" }}
              >
                User List
              </Typography>
            </Box>
            <Box className={styles.sortButtonContainer}>
              <Button variant="contained" onClick={() => usernameOrderHandler(isASC)}>{isASC ? `ASC` : `DESC`}</Button>
            </Box>
          </Box>
          <Box className={styles.headerTwoBox2}>
            <Button variant="contained" onClick={() => setIsModalShowing(true)} >Add User</Button>
          </Box>
        </Box>
        <Box className={styles.gridContainer}>
          <DataGrid
            rows={SAVED_USERS}
            columns={columns}
            pageSize={pageSize}
            onPageSizeChange={(pageSize) => setPageSize(pageSize)}
            rowsPerPageOptions={[5, 10, 15]}
            disableSelectionOnClick
          />
        </Box>
      </Box>

    </Box>
  );
}

export default App;
