import * as actions from "../actions/actionConstants"
export const usersAdded = (value) => {
    return {
        type: actions.USERS_ADDED,
        payload: value
    }
}

export const userAdded = (value) => {
    return {
        type: actions.USER_ADDED,
        payload: value
    }
}
export const userUpdated = (value) => {
    return {
        type: actions.USER_UPDATED,
        payload: value
    }
}
export const userRemoved = (value) => {
    return {
        type: actions.USER_REMOVED,
        payload: value
    }
}

export const selectedUser = (value) => {
    return {
        type: actions.SELECTED_USER,
        payload: value
    }
}
export const sortUsersAZ = (value) => {
    return {
        type: actions.SORT_USERS_AZ,
        payload: value
    }
}
export const sortUsersZA = (value) => {
    return {
        type: actions.SORT_USERS_ZA,
        payload: value
    }
}