export const USERS_ADDED = "usersAdded"
export const USER_ADDED = "userAdded";
export const USER_UPDATED = "userUpdated";
export const USER_REMOVED = "userRemoved";
export const SELECTED_USER = "selectedUser";
export const SORT_USERS_AZ = "sortUsersAZ";
export const SORT_USERS_ZA = "sortUsersZA";