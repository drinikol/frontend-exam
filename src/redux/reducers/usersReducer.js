import * as actions from "../actions/actionConstants"
const usersReducer = (state = [], { type, payload }) => {
    switch (type) {
        case actions.USERS_ADDED: {
            return [
                ...payload
            ]
        }
        case actions.USER_ADDED: {
            return [
                ...state,
                {
                    ...payload
                }
            ]
        }
        case actions.USER_REMOVED: {
            return state.filter(user => user.id !== payload.id)
        }
        case actions.USER_UPDATED: {
            return state.map(user => user.id !== payload.id ? user : { ...user, ...payload })
        }
        case actions.SORT_USERS_AZ: {
            return state.slice().sort(function (a, b) {
                var unameA = a.username.toLowerCase(),
                    unameB = b.username.toLowerCase()
                if (unameA < unameB)
                    return -1
                if (unameA > unameB)
                    return 1
                return 0
            })
        }
        case actions.SORT_USERS_ZA: {
            return state.slice().sort(function (a, b) {
                var unameA = a.username.toLowerCase(),
                    unameB = b.username.toLowerCase()
                if (unameA > unameB)
                    return -1
                if (unameA < unameB)
                    return 1
                return 0
            })
        }
        default: {
            return state;
        }
    }
}
export default usersReducer