import usersReducer from "./usersReducer";
import selectedReducer from "./selectedReducer";
import { combineReducers } from "redux";

const masterReducer = combineReducers({
    users: usersReducer,
    selectedUser: selectedReducer
})

export default masterReducer