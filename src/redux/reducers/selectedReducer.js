import * as actions from "../actions/actionConstants"
const selectedReducer = (state = {}, { type, payload }) => {
    switch (type) {
        case actions.SELECTED_USER: {
            return { ...payload }
        }
        default: {
            return state;
        }
    }
}
export default selectedReducer